import React from "react";
// import Header from "../Header/Header";
import Banner from "../Banner/Banner";
import About from "../About/About";
import Footer from "../Footer/Footer";
// import { Container, Row, Col } from "react-bootstrap";

const home = () => (

    <div>
        <Banner />
        <About />
        <Footer />
    </div>

)

export default home;