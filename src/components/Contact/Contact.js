import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import "./Contact.css";

const initialState = {
    name: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: '',
    phoneNumber: '',

    nameError: '',
    lastNameError: '',
    emailError: '',
    passwordError: '',
    confirmPasswordError: '',
    phoneNumberError: '',
    passwordDontMatchError: ''
}

class Contact extends Component {
    //danger

    constructor(props) {
        super(props);

        this.state = initialState;

        this.handleChangeInput = this.handleChangeInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validate = this.validate.bind(this);

    }

    handleChangeInput = (event) => {
        this.setState({ [event.target.name]: event.target.value });
        // console.log("Chandru:" , event.target);
        const isValid = this.validate();
        if (isValid) {
        }
    };

    validate = () => {
        let nameError = "";
        let lastNameError = "";
        let emailError = "";
        let passwordError = "";
        let confirmPasswordError = "";
        let phoneNumberError = "";
        let passwordDontMatchError = "";

        const nameIsValid = /^[A-Za-z]+$/;
        const emailIsValid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        const passwordIsValid = /^[A-Za-z]\w{7,14}$/;
        const phoneNumberIsValid = /^\d{10}$/;

        if (!this.state.name.match(nameIsValid)) {
            nameError = "Invalid Name";
        }

        if (!this.state.lastName.match(nameIsValid)) {
            lastNameError = "Invalid Last Name";
        }

        if (!this.state.email.match(emailIsValid)) {
            emailError = 'Invalid email';
        }

        if (!this.state.password.match(passwordIsValid)) {
            passwordError = 'Invalid password';
        }

        if (!this.state.confirmPassword.match(passwordIsValid)) {
            confirmPasswordError = "Please provide same password";
        }
        if (this.state.confirmPassword !== this.state.password) {
            passwordDontMatchError = "Passwords should be same";
        }

        if (!this.state.phoneNumber.match(phoneNumberIsValid)) {
            phoneNumberError = 'Invalid Phone Number';
        }


        if (nameError || lastNameError || emailError || passwordError || confirmPasswordError || passwordDontMatchError || phoneNumberError) {
            this.setState({ nameError, lastNameError, emailError, passwordError, confirmPasswordError, passwordDontMatchError, phoneNumberError });
            return false;
        }

        return true;
    }




    handleSubmit = (event) => {
        event.preventDefault();

        const isValid = this.validate();
        if (isValid) {
            alert("Submitted!");
           
            this.setState({ initialState });
        }

    }


    
    render() {
        return (
            <Container className="mt-5 pt-5">
                <Row>
                    <Col xs={2}></Col>
                    <Col className="shadow-sm p-5 mb-5 bg-white rounded" xs={8}>
                        <Form onSubmit={this.handleSubmit}>
                            <Row>
                                <Col>
                                    <Form.Control
                                        name='name'
                                        placeholder="First name"
                                        onChange={this.handleChangeInput}
                                        value={this.state.name}
                                    />
                                    <span className='invalid-feedback'>{this.state.nameError}</span>

                                </Col>
                                <Col>
                                    <Form.Control
                                        name="lastName"
                                        placeholder="Last name"
                                        onChange={this.handleChangeInput}
                                        value={this.state.lastName}
                                    />
                                    <span className='invalid-feedback'>{this.state.lastNameError}</span>

                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col>
                                    <Form.Control
                                        name="email"
                                        placeholder="Email"
                                        onChange={this.handleChangeInput}
                                        value={this.state.email}
                                    />
                                    <span className='invalid-feedback'>{this.state.emailError}</span>

                                </Col>
                                <Col>
                                    <Form.Control
                                        name="password"
                                        type="password"
                                        placeholder="Password"
                                        onChange={this.handleChangeInput}
                                        value={this.state.password}
                                    />
                                    <span className='invalid-feedback'>{this.state.passwordError}</span>
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col>
                                    <Form.Control
                                        name="confirmPassword"
                                        type="password"
                                        placeholder="Confirm Password"
                                        onChange={this.handleChangeInput}
                                        value={this.state.confirmPassword}
                                    />
                                    <span className='invalid-feedback'>{this.state.confirmPasswordError}</span>
                                    <span className='invalid-feedback'>{this.state.passwordDontMatchError}</span>
                                </Col>
                                <Col>
                                    <Form.Control
                                        name="phoneNumber"
                                        type="number"
                                        placeholder="Phone Number"
                                        onChange={this.handleChangeInput}
                                        value={this.state.phoneNumber}
                                    />
                                    <span className='invalid-feedback'>{this.state.phoneNumberError}</span>
                                </Col>
                            </Row>

                            <Button type="submit" className="mt-4"> Submit </Button>
                        </Form>
                    </Col>
                    <Col xs={2}></Col>
                </Row>
            </Container>
        )
    }

}

export default Contact;