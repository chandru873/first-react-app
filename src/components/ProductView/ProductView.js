import React from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import monginis from "../../assets/images/monginis.jpg";
import ProductsData from "../ProductsData";
import Products from "../Products/Products";

const productView = ({ id, title, desc, price, img }, props) => {

        return (
            <div>
                <Container className="mt-5 pt-5">
                    <Row id={Products.id}>
                        <Col xs={5}>
                            <img src={props.img} width="400" />
                        </Col>
                        <Col xs={7}>
                            <h2>{title}</h2>
                            <p>{desc}</p>
                            <h3>RS: {price}</h3>
                            <Button type="button" className="success" >Add to Cart</Button>
                            <Button type="button" className="primary ml-4" >Buy Now</Button>
                        </Col>
                    </Row>
                </Container>
            </div>
        )

        
    
}

export default productView;