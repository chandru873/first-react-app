import React from "react";
import { Container, Row, Col } from "react-bootstrap";

const about = (props) => (
    <div>
        <Container className="mt-5">
            <Row>
                <Col xs="1" />
                <Col className="text-center">
                    <h2 className="font-weight-100">I am a UI UX Designer and Developer from India living in Bangalore</h2>
                    <p className="mt-3 font-weight-300 font-size-18">Starting with basic websites several years ago I found myself working as an UI UX designer and developer for complex projects with a holistic approach soon. Visually appealing designs, subtle details and brand guidelines combined to innovative interfaces across various touch points became my daily companion.</p>
                </Col>
                <Col xs="1" />
            </Row>
        </Container>
    </div>
)

export default about;