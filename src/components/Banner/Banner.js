import React from 'react';
import { Parallax } from 'react-parallax';
import "./Banner.css";
import logo from "../../assets/images/logo.png";


const banner = () => (
    <div>
        {/* -----basic config-----*/}
        <Parallax
            className="parallex"
            blur={0}
            bgImage={require('../../assets/images/parallex.jpg')}
            bgImageAlt="Banner"
            strength={100}
        >
            <div className="white text-center vertical-center ">
                <img src={logo} alt={logo}  />
                <p>I am R.C.Gupta</p>
            </div>
            <div style={{ height: '100vh' }} />
        </Parallax>

        {/* -----dynamic blur-----*/}
        {/* <Parallax
            blur={{ min: -15, max: 15 }}
            bgImage={require('../../assets/images/parallex.jpg')}
            bgImageAlt="the dog"
            strength={-200}
        >
            Blur transition from min to max
            <div style={{ height: '100vh' }} />
        </Parallax> */}
    </div>
);
export default banner;