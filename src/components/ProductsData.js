
import React, { Component } from "react";
import Monginis from "../assets/images/monginis.jpg";
import DTP from "../assets/images/dtp.jpg";
import BBM from "../assets/images/bmm.jpg";
import Edge from "../assets/images/edge.jpg";
import Skincity from "../assets/images/skincity.jpg";
import Oconnors from "../assets/images/oconnors.jpg";
import Products from "./Products/Products";
import ProductView from "./ProductView/ProductView";
import { Container, Row, Col, Button } from "react-bootstrap";
import { Route, Link, BrowserRouter, Router } from 'react-router-dom';



export const productsData =
    [
        {
            id: 1,
            title: 'Monginis',
            desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",
            price: 110,
            img: Monginis,
            inCart: false,
            count: 0,
            total: 0
        },
        {
            id: 2,
            title: 'DTP',
            desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",
            price: 80,
            img: DTP,
            inCart: false,
            count: 0,
            total: 0
        },
        {
            id: 3,
            title: 'BBM',
            desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",
            price: 120,
            img: BBM,
            inCart: false,
            count: 0,
            total: 0
        },
        {
            id: 4,
            title: 'Edge',
            desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",
            price: 260,
            img: Edge,
            inCart: false,
            count: 0,
            total: 0
        },
        {
            id: 5,
            title: 'Skincity',
            desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",
            price: 160,
            img: Skincity,
            inCart: false,
            count: 0,
            total: 0
        },
        {
            id: 6,
            title: 'Oconnors',
            desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",
            price: 90,
            img: Oconnors,
            inCart: false,
            count: 0,
            total: 0
        }
    ];


export const detailProduct = {
    id: 1,
    title: 'Oconnors',
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",
    price: 90,
    img: Oconnors
};



