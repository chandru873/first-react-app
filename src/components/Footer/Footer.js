import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import facebook from "../../assets/icons/facebook-f-brands.svg";
import twitter from "../../assets/icons/twitter-brands.svg";



const footer = () => (
    <footer className="footer">
        <Container>
            <Row>
            <Col xs={6}>
            <ul className="list-unstyled">
                    <li><img src={facebook} width="20" /></li>
                    <li><img src={twitter} width="20" /></li>
                    <li></li>
                </ul>
                
            </Col>
            <Col xs={6}>
                    
            </Col>
            </Row>
        </Container>
    </footer>
);

export default footer;