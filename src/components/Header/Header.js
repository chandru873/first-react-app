import React from "react";
import { Nav, Navbar, Image } from "react-bootstrap";
import logo from "../../assets/images/logo.png";
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import Home from "../Home/Home";
import About from "../About/About";
import Contact from "../Contact/Contact";
// import Products from "../Products/Products";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import ProductView from "../ProductView/ProductView";
// import ProductData from "../ProductsData";
import ProductDetails from "../ProductDetails/ProductDetails";


const header = () => {

    return (
        <Router>
            <Navbar className="bg-light py-3" expand="lg" fixed="top">
                <Navbar.Brand href="#home">
                    <Image src={logo} width="20" />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link><Link to="/Home">Home</Link></Nav.Link>
                        <Nav.Link><Link to="/About">About Us</Link></Nav.Link>
                        <Nav.Link><Link to="/ProductDetails">Products</Link></Nav.Link>
                        <Nav.Link><Link to="/Contact">Contact Us</Link></Nav.Link>
                        {/* <Nav.Link><Link to="/ProductView">Product View</Link></Nav.Link> */}
                    </Nav>

                    <Nav.Link href="#link" className="mr-sm-2">Login</Nav.Link>
                    <Nav.Link><Link to="/Contact">
                        <div className="position-relative">
                            <FontAwesomeIcon icon={faShoppingCart} style={{ color: 'blue' }} />
                            <span className="top-number">1</span>
                            <span className="ml-2">Cart</span>
                        </div>
                        
                    </Link>
                    </Nav.Link>
                    <Nav.Link href="#link" className="mr-sm-2">Checkout</Nav.Link>
                </Navbar.Collapse>
            </Navbar>

            <Route exact path="/Home" component={Home} />
            <Route exact path="/About" component={About} />
            <Route exact path="/ProductDetails" component={ProductDetails} />
            <Route exact path="/Contact" component={Contact} />
            <Route exact path="/ProductView" component={ProductView} />
        </Router>
    )

}

export default header;