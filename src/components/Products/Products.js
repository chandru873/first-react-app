import React from "react";
import { Container, Row, Col, Card, CardDeck, Button } from "react-bootstrap";
import Monginis from "../../assets/images/monginis.jpg";
// import DTP from "../../assets/images/dtp.jpg";
// import Edge from "../../assets/images/edge.jpg";
import ProductView from "../ProductView/ProductView";
import { Route, Link, BrowserRouter, Router } from 'react-router-dom';
// import ProductData from "../ProductsData";
import ProductDetails from "../ProductDetails/ProductDetails";

const products = ({ id, title, desc, price, img, inCart }) => (


    <Col xs={4} className="mt-4">
        <CardDeck onClick={() => console.log("Clicked!")}>

            <Card id={id}>
                <Link to="/ProductView" >
                    <Card.Img variant="top" src={img} />
                </Link>
                <Card.Body>
                    <Card.Title>{title}</Card.Title>
                    <Card.Text>
                        {desc}
                    </Card.Text>
                    <span>Rs: {price}</span>
                </Card.Body>
                <Card.Footer>
                    <Button
                        type="button"
                        className="success"
                        disabled={inCart ? true : false}
                        onClick={() => { console.log("Added to the Card!") }} >
                        
                        {inCart ? (<span disabled> In Cart</span>) : (<span>Add To Cart</span>)}

                    </Button>
                    <Button type="button" className="primary float-right" >Buy Now</Button>
                </Card.Footer>
            </Card>

            <Route exact path="/ProductView" component={ProductView} />
        </CardDeck>
    </Col>

)

export default products;