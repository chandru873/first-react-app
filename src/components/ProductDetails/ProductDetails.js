import React, {Component} from "react";
import ProductView from "../ProductView/ProductView";
import {productsData, detailProduct} from "../ProductsData";
import Products from "../Products/Products";
import {Container, Row, Col} from "react-bootstrap";

class prodsuctDetails extends Component {
   
    
    state = {
        products: productsData,
        detailProduct: detailProduct
    }

    handleDetail = () => {
        console.log("Hello! from  Detail");
    }

    addToCart = () => {
        console.log("Add To Cart!");
    }

    render() {
        return (
            <Container className="mt-5 pt-5">

                {/* <Row>
                    <Products.Provider value = {{
                        ...this.state,
                        handleDetail: this.handleDetail,
                        addToCart: this.addToCart
                    }}>
                        {this.props.children}
                    </Products.Provider>
                </Row> */}


                <Row>
                    {productsData.map(p => <Products key={p.id} {...p} />)}
                </Row>
            </Container>

        )
    };
}


export default prodsuctDetails;